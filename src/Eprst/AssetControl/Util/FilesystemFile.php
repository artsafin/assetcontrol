<?php
namespace Eprst\AssetControl\Util;

use Eprst\AssetControl\Aom\RawFile;

class FilesystemFile implements RawFile
{
    /**
     * @var Path
     */
    private $path;

    public function __construct($path)
    {
        $this->path = new Path($path);
    }

    /**
     * @return string
     */
    public function loadContent()
    {
        $result = file_get_contents((string)$this->path);
        if ($result === false) {
            throw new \RuntimeException("Unable to read {$this->path}: " . json_encode(error_get_last()));
        }
        return $result;
    }

    /**
     * @return Path
     */
    public function getPath()
    {
        return $this->path;
    }

    public function replace(Position $position, $string, Path $path = null)
    {
        $content = $this->loadContent();
        $content = substr_replace($content, $string, $position->getStartOffset(), $position->getLength());

        $path = $path ?: $this->path;
        if (file_put_contents($path, $content, LOCK_EX) === false) {
            throw new \RuntimeException("Unable to write to {$path}");
        }
    }
}