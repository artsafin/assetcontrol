<?php


namespace Eprst\AssetControl\Ui\Input;

use Eprst\AssetControl\App\Parameters;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputOption;

class Input implements Parameters
{
    /**
     * @var InputOption[]|InputArgument[]
     */
    protected $def = [];

    public function assetRoot()
    {
        $this->def[] = new InputOption(self::ASSET_ROOT,
                                       null,
                                       InputOption::VALUE_REQUIRED,
                                       'Asset root. If not specified, defaults to directory of first target. Relative to current directory.');

        return $this;
    }

    public function compileDir()
    {
        $this->def[] = new InputOption(self::COMPILE_DIR,
                                       null,
                                       InputOption::VALUE_REQUIRED,
                                       'Compile directory. Relative to asset root.',
                                       'compiled');

        return $this;
    }

    public function targets()
    {
        $this->def[] = new InputArgument(self::TARGETS,
                                         InputArgument::IS_ARRAY | InputArgument::REQUIRED,
                                         'Targets to analyze. Can be a path with wildcards. Relative to current directory.');

        return $this;
    }

    public function compileVirtualPath()
    {
        $this->def[] = new InputOption(self::COMPILE_VIRTUAL_PATH,
                                       null,
                                       InputOption::VALUE_REQUIRED,
                                       'Prefix for virtual paths of generated assets.',
                                       '/compiled');

        return $this;
    }

    public function dryRun()
    {
        $this->def[] = new InputOption(self::DRY_RUN,
                                       null,
                                       InputOption::VALUE_NONE,
                                       'If specified, no changes will be made to any files.');

        return $this;
    }

    public static function requireInput()
    {
        return new self();
    }

    public function createDefinition()
    {
        return new InputDefinition($this->def);
    }

    public function mergeInto(InputDefinition $other)
    {
        $def = $this->createDefinition();

        $other->addArguments($def->getArguments());
        $other->addOptions($def->getOptions());

        return $this;
    }

    public function transformTool()
    {
        $this->def[] = new InputOption(self::TRANSFORM_TOOL,
                                       null,
                                       InputOption::VALUE_REQUIRED,
                                       'Internal alias of tool to use for transformation. If omitted, '
                                       . self::TRANSFORM_TOOL_CMD
                                       . ' option is required.', 'cmd');

        return $this;
    }

    public function transformToolCmd()
    {
        $this->def[] = new InputOption(self::TRANSFORM_TOOL_CMD,
                                       null,
                                       InputOption::VALUE_REQUIRED,
                                       'Command which will be used to make transformation. '
                                       . 'Value is interpolated with arguments, i.e. :' . self::ASSET_ROOT . ': will be replaced with actual passed value.');

        return $this;
    }
}