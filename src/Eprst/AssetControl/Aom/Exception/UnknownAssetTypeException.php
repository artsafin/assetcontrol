<?php


namespace Eprst\AssetControl\Aom\Exception;

/**
 * Class UnknownAssetTypeException
 *
 * @codeCoverageIgnore
 */
class UnknownAssetTypeException extends \Exception
{

}