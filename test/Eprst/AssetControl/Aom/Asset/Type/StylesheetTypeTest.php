<?php


namespace Test\Eprst\AssetControl\Aom\Asset\Type;


use Eprst\AssetControl\Aom\Asset\AssetReference;
use Eprst\AssetControl\Aom\Asset\Type\JavascriptType;
use Eprst\AssetControl\Aom\Asset\Type\StylesheetType;
use Eprst\AssetControl\Util\Path;

class StylesheetTypeTest extends \PHPUnit_Framework_TestCase
{
    public function testToHtml()
    {
        $a = new AssetReference(new Path('/home/test.js'), new Path('test.js'));
        $t = new StylesheetType();

        self::assertEquals('<link rel="stylesheet" type="text/css" href="test.js"></link>', (string) $t->toHtml($a));
    }

    public function testGetExtension()
    {
        $t = new StylesheetType();

        self::assertEquals('css', $t->getExtension());
        self::assertEquals('.css', $t->getExtension(true));
    }

    public function testEquals()
    {
        $t = new StylesheetType();

        self::assertTrue($t->equals(new StylesheetType()));
        self::assertFalse($t->equals(new JavascriptType()));
    }
}
