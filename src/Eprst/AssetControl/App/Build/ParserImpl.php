<?php

namespace Eprst\AssetControl\App\Build;

use DOMNamedNodeMap;
use DOMNode;
use DOMXPath;
use Eprst\AssetControl\Aom\Asset\Asset;
use Eprst\AssetControl\Aom\Asset\AssetReference;
use Eprst\AssetControl\Aom\Asset\InlineAsset;
use Eprst\AssetControl\Aom\Asset\Type\JavascriptType;
use Eprst\AssetControl\Aom\Asset\Type\StylesheetType;
use Eprst\AssetControl\Aom\AssetFile;
use Eprst\AssetControl\Aom\FileParser;
use Eprst\AssetControl\Aom\RawFile;
use Eprst\AssetControl\App\CleanupManager;
use Eprst\AssetControl\Util\Path;
use Eprst\AssetControl\Util\Position;

class ParserImpl implements FileParser
{
    private $commentHeader;
    /**
     * @var CleanupManager
     */
    private $cleanupManager;

    public function __construct($commentHeader, CleanupManager $cleanupManager)
    {
        $this->commentHeader  = $commentHeader;
        $this->cleanupManager = $cleanupManager;
    }

    public function parse(RawFile $file, Path $assetRoot)
    {
        $chunks = [];

        foreach ($this->findChunkAreas($file) as $chunkBuilder) {
            $relPath = $chunkBuilder->getChunkRelativeBasePath() ?:
                $file->getPath()->getDiff($assetRoot)->getDeepestDirectory();
            $assets  = $this->findAssets($chunkBuilder->getText(), $assetRoot, $relPath);
            $chunkBuilder->addAssets($assets);

            $chunks = array_merge($chunks, $chunkBuilder->build());
        }

        return new AssetFile($file, $chunks);
    }

    /**
     * @param RawFile $file
     *
     * @return ChunkBuilder[]
     */
    private function findChunkAreas($file)
    {
        $re = '<!--\s*' . $this->commentHeader . '(.*?)-->'
              . '(.+?)'
              . '<!--\s*/' . $this->commentHeader . '\s*-->';

        $result = [];

        $content = $file->loadContent();

        if (preg_match_all("|{$re}|su", $content, $matches, PREG_OFFSET_CAPTURE | PREG_SET_ORDER)) {
            foreach ($matches as $match) {
                $chunkParams = isset($match[1][0]) ? $this->decodeChunkParams($match[1][0]) : [];

                if (isset($match[2])) {
                    list($str, $offset) = $match[2];
                    $result[] = (new ChunkBuilder())
                        ->configure($chunkParams)
                        ->setText($str)
                        ->setPosition(new Position($offset, strlen($str)));
                }
            }
        }

        return $result;
    }

    private function decodeChunkParams($raw)
    {
        $json = json_decode($raw, true, 2);
        if (json_last_error() == JSON_ERROR_NONE) {
            return $json;
        }

        return [];
    }

    /**
     * @param string $chunkText
     * @param Path   $assetRoot
     * @param Path   $relPath
     *
     * @return Asset[]
     */
    private function findAssets($chunkText, Path $assetRoot, Path $relPath)
    {
        $dom = new \DOMDocument();
        $dom->loadHTML($chunkText);
        $xpath = new \DOMXPath($dom);

        return array_merge(
            $this->findJs($xpath, $assetRoot, $relPath),
            $this->findCss($xpath, $assetRoot, $relPath)
        );
    }

    private function makeRealPath(DOMNamedNodeMap $attrs, Path $virtual, Path $assetRoot, Path $relPath)
    {
        $relativeBasePath = $this->tryExtractBasePath($attrs) ?: $relPath;
        $relativeBasePath = $relativeBasePath->toAbsolute($assetRoot);

        return $virtual->isRelative() ? $virtual->prefixWith($relativeBasePath) : $virtual->prefixWith($assetRoot);
    }

    private function tryExtractBasePath(DOMNamedNodeMap $attrs)
    {
        if ($attr = $attrs->getNamedItem('ac:relative-to')) {
            return new Path($attr->nodeValue);
        }

        return null;
    }

    private function findJs(DOMXPath $xpath, Path $assetRoot, Path $relPath)
    {
        $result = [];

        /** @var DOMNode $node */
        foreach ($xpath->query('//script') as $node) {
            if (($srcAttr = $node->attributes->getNamedItem('src')) && $srcAttr->nodeValue) {
                $virtualPath = new Path($srcAttr->nodeValue);

                $realPath = $this->makeRealPath($node->attributes, $virtualPath, $assetRoot, $relPath);

                $result[] = new AssetReference($realPath, $virtualPath);
            } else if (trim($node->textContent)) {
                $result[] = new InlineAsset(trim($node->textContent), new JavascriptType(),
                    function (InlineAsset $asset) {
                        $this->cleanupManager->removeFile($asset->getRealPath());
                    });
            }
        }

        return $result;
    }

    private function findCss(DOMXPath $xpath, Path $assetRoot, Path $relPath)
    {
        $result = [];

        /** @var DOMNode $node */
        foreach ($xpath->query('//link[@rel="stylesheet"]') as $node) {
            if (($srcAttr = $node->attributes->getNamedItem('href')) && $srcAttr->nodeValue) {
                $virtualPath = new Path($srcAttr->nodeValue);
                $realPath    = $this->makeRealPath($node->attributes, $virtualPath, $assetRoot, $relPath);
                $result[]    = new AssetReference($realPath, $virtualPath);
            } else if (trim($node->textContent)) {
                $result[] = new InlineAsset(trim($node->textContent), new StylesheetType(),
                    function (InlineAsset $asset) {
                        $this->cleanupManager->removeFile($asset->getRealPath());
                    });
            }
        }

        return $result;
    }
}