<?php


namespace Eprst\AssetControl\Aom\Asset;


use Eprst\AssetControl\Aom\Asset\Type\Type;
use Eprst\AssetControl\Util\Html;
use Eprst\AssetControl\Util\Path;

interface Asset
{
    /**
     * @return Type
     */
    public function getType();

    /**
     * @return string
     */
    public function getContent();

    /**
     * @return Path
     */
    public function getVirtualPath();

    /**
     * @return Path
     */
    public function getRealPath();

    /**
     * @return Html
     */
    public function toHtml();
}