<?php


namespace Eprst\AssetControl\App\Transform\Cmd;


use Eprst\AssetControl\Aom\Asset\Asset;
use Eprst\AssetControl\Aom\Asset\AssetReference;
use Eprst\AssetControl\Aom\Asset\TransformedAsset;
use Eprst\AssetControl\Aom\Asset\Type\Type;
use Eprst\AssetControl\Aom\Chunk;
use Eprst\AssetControl\App\CleanupManager;
use Eprst\AssetControl\App\Parameters;
use Eprst\AssetControl\App\Transform\TransformAdapter;
use Eprst\AssetControl\Util\Path;
use Symfony\Component\Process\Process;

class CmdAdapter implements TransformAdapter
{
    /**
     * @var
     */
    private $cmd;
    /**
     * @var CleanupManager
     */
    private $cleanupManager;

    /**
     * @param string         $cmd
     * @param CleanupManager $cleanupManager
     */
    public function __construct($cmd, CleanupManager $cleanupManager)
    {
        $this->cmd            = $cmd;
        $this->cleanupManager = $cleanupManager;
    }


    /**
     * @param Chunk      $chunk
     * @param Parameters $conf
     *
     * @return Asset[]
     */
    public function compile(Chunk $chunk, Parameters $conf)
    {
        $result = [];

        /** @var Asset[] $assets */
        foreach ($chunk->getAssetsByType() as $assets) {
            $args = array_map(function($asset){
                /** @var Asset $asset */
                return escapeshellarg($asset->getRealPath()->toNativeString());
            }, $assets);

            if (!$args) {
                continue;
            }

            $cmd = sprintf("%s %s", $this->cmd, implode(' ', $args));
            $result[] = $this->run($cmd, $assets[0]->getType(), $conf);
        }

        return $result;
    }

    private function run($cmd, Type $type, Parameters $conf)
    {
        echo 'Executing command: ', $cmd, PHP_EOL;

        $process = new Process($cmd);
        $process->mustRun();

        $content = $process->getOutput();

        return new TransformedAsset($type, $content, $conf->compileDir(), $conf->compileVirtualPath());
    }
}