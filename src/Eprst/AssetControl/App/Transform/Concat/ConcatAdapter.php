<?php


namespace Eprst\AssetControl\App\Transform\Concat;


use Eprst\AssetControl\Aom\Asset\Asset;
use Eprst\AssetControl\Aom\Asset\TransformedAsset;
use Eprst\AssetControl\Aom\Chunk;
use Eprst\AssetControl\App\CleanupManager;
use Eprst\AssetControl\App\Parameters;
use Eprst\AssetControl\App\Transform\TransformAdapter;

class ConcatAdapter implements TransformAdapter
{
    /**
     * @var string
     */
    private $cmd;
    /**
     * @var CleanupManager
     */
    private $cleanupManager;

    /**
     * @param string         $cmd
     * @param CleanupManager $cleanupManager
     */
    public function __construct($cmd, CleanupManager $cleanupManager)
    {
        $this->cmd            = $cmd;
        $this->cleanupManager = $cleanupManager;
    }

    /**
     * @param Chunk      $chunk
     * @param Parameters $conf
     *
     * @return Asset[]
     */
    public function compile(Chunk $chunk, Parameters $conf)
    {
        $result = [];

        /** @var Asset[] $assets */
        foreach ($chunk->getAssetsByType() as $assets) {
            if (!$assets) {
                continue;
            }

            $result[] = $this->concat($assets, $conf);
        }

        return $result;
    }

    /**
     * @param Asset[]    $assets
     * @param Parameters $conf
     *
     * @return TransformedAsset
     */
    private function concat($assets, Parameters $conf)
    {
        $content = '';

        foreach ($assets as $a) {
            $content .= file_get_contents($a->getRealPath());
        }

        $type = $assets[0]->getType();

        return new TransformedAsset($type, $content, $conf->compileDir(), $conf->compileVirtualPath());
    }
}