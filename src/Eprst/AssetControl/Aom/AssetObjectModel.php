<?php


namespace Eprst\AssetControl\Aom;


class AssetObjectModel
{
    /**
     * @var AssetFile[]
     */
    private $assetFiles;

    function __construct($assetFiles)
    {
        $this->assetFiles = $assetFiles;
    }

    /**
     * @return AssetFile[]
     */
    public function getAssetFiles()
    {
        return $this->assetFiles;
    }
}