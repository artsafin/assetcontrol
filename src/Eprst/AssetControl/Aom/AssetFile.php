<?php


namespace Eprst\AssetControl\Aom;

use Eprst\AssetControl\Util\Path;

class AssetFile
{
    /**
     * @var Chunk[]
     */
    private $chunks;

    private $replacements;

    /**
     * @var RawFile
     */
    private $file;

    public function __construct(RawFile $file, $chunks)
    {
        $this->chunks = $chunks;
        $this->file   = $file;
    }

    public function replace(Chunk $old, Chunk $new, Path $path = null)
    {
        $this->replacements[] = [$old, $new];

        $this->file->replace($old->getPosition(), $new->toHtmlString(), $path);
    }

    public function getPath()
    {
        return $this->file->getPath();
    }

    /**
     * @return Chunk[]
     */
    public function getChunks()
    {
        return $this->chunks;
    }

    /**
     * @return Chunk[][]
     */
    public function getReplacements()
    {
        return $this->replacements;
    }
}