<?php


namespace Eprst\AssetControl\Aom\Asset;


use Eprst\AssetControl\Aom\Asset\Type\Type;
use Eprst\AssetControl\Aom\Exception\AssetRealizationException;
use Eprst\AssetControl\Util\Html;
use Eprst\AssetControl\Util\Path;

class TransformedAsset implements Asset
{
    /**
     * @var Type
     */
    private $type;
    /**
     * @var
     */
    private $content;

    /**
     * @var bool
     */
    private $isSaved;

    /**
     * @var Path
     */
    private $realPath;

    /**
     * @var Path
     */
    private $virtualPath;

    public function __construct(Type $type, $content, Path $resultDir, Path $virtualPathPrefix)
    {
        $this->type = $type;

        $fileName      = new Path(sha1($content) . $type->getExtension(true));
        $this->content = $content;

        $this->realPath    = $fileName->toAbsolute($resultDir);
        $this->virtualPath = $fileName->prefixWith($virtualPathPrefix);

        $this->isSaved = false;
    }

    /**
     * @return Type
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @return Path
     */
    public function getVirtualPath()
    {
        return $this->virtualPath;
    }

    private function realizeAsset()
    {
        if (!file_exists($this->realPath)) {
            if (!is_dir($this->realPath->getDeepestDirectory())) {
                if (false === @mkdir($this->realPath->getDeepestDirectory(), 0777, true)) {
                    throw new AssetRealizationException("Unable to create directory: {$this->realPath->getDeepestDirectory()}: "
                                                        . json_encode(error_get_last()));
                }
            }

            if (false === @file_put_contents($this->realPath, $this->content)) {
                throw new AssetRealizationException("Unable to write to {$this->realPath}: "
                                                    . json_encode(error_get_last()));
            }
        }

        return true;
    }

    /**
     * @return Path
     */
    public function getRealPath()
    {
        if (!$this->isSaved) {
            $this->isSaved = $this->realizeAsset();
        }

        return $this->realPath;
    }

    /**
     * @return Html
     */
    public function toHtml()
    {
        return $this->type->toHtml($this);
    }
}