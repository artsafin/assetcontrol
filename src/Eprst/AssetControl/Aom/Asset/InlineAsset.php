<?php


namespace Eprst\AssetControl\Aom\Asset;


use Eprst\AssetControl\Aom\Asset\Type\Type;
use Eprst\AssetControl\Aom\Exception\AssetRealizationException;
use Eprst\AssetControl\Util\Path;

class InlineAsset implements Asset
{
    /**
     * @var string
     */
    private $content;

    /**
     * @var Path
     */
    private $fsPath;
    /**
     * @var Type
     */
    private $type;
    /**
     * @var callable
     */
    private $onCreateFile;

    public function __construct($content, Type $type, callable $onCreateFile)
    {
        $this->content      = $content;
        $this->hash         = md5($content);
        $this->type         = $type;
        $this->onCreateFile = $onCreateFile;
    }


    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @return Path
     */
    public function getVirtualPath()
    {
        return $this->hash . $this->getType()->getExtension(true);
    }

    /**
     * @return Path
     * @throws AssetRealizationException
     */
    public function getRealPath()
    {
        if ($this->fsPath === null) {
            $this->fsPath = new Path(implode('', [sys_get_temp_dir(), DIRECTORY_SEPARATOR, $this->getVirtualPath()]));
            $putResult    = @file_put_contents($this->fsPath, $this->content);
            if ($putResult === false) {
                throw new AssetRealizationException($this->fsPath);
            }
            call_user_func($this->onCreateFile, $this);
        }

        return $this->fsPath;
    }

    /**
     * @return Type
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function toHtml()
    {
        return (string)$this->type->toHtml($this);
    }
}