<?php


namespace Eprst\AssetControl\Aom\Asset\Type;


use Eprst\AssetControl\Aom\Exception\UnknownAssetTypeException;
use Eprst\AssetControl\Util\Path;

class TypeFactory
{
    public static function createByExtension(Path $path)
    {
        switch ($path->getExtension()) {
            case 'js':
                return new JavascriptType();
            case 'css':
                return new StylesheetType();
            default:
                throw new UnknownAssetTypeException("Unable to guess asset type by extension: {$path->getExtension()}");
        }
    }
}