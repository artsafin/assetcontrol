<?php


namespace Test\Eprst\AssetControl\Util;


use Eprst\AssetControl\Util\Html;

class HtmlTest extends \PHPUnit_Framework_TestCase
{
    public function testCastToStringResultsWithHtmlTag()
    {
        $result = Html::script();

        self::assertNotInternalType('string', $result);
        self::assertInternalType('string', (string) $result);
    }

    public function testStaticCallCreatesEmptyTag()
    {
        self::assertEquals('<script></script>', (string) Html::script());
        self::assertEquals('<qwerty></qwerty>', (string) Html::qwerty());
        self::assertEquals('<link></link>', (string) Html::link());
    }

    public function testStaticCallWithArgumentsCreatesAttributes()
    {
        self::assertEquals('<script src="http://example.com/jquery.js" type="text/javascript"></script>',
                           (string)Html::script(['src' => 'http://example.com/jquery.js', 'type' => 'text/javascript']));
        self::assertEquals('<link rel="stylesheet"></link>',
                           (string)Html::link(['rel' => 'stylesheet']));
    }

    public function testMagicCallWithUnderscoreCreatesAttributes()
    {
        self::assertEquals('<script foo="bar"></script>',
                           (string)Html::script()->_foo('bar'));

        self::assertEquals('<script src="http://example.com/jquery.js" foo="bar"></script>',
                           (string)Html::script(['src' => 'http://example.com/jquery.js'])->_foo('bar'));

        self::assertEquals('<script src="http://example.com/jquery.js" foo="bar"></script>',
                           (string)Html::script(['src' => 'http://example.com/jquery.js'])->_foo('bar')->baz('qux'));
    }

    public function testEmptyTagAndContentEqualsEmpty()
    {
        self::assertEquals('', new Html(null, [], null));
    }

    public function testEmptyTagAndContentEqualsContent()
    {
        self::assertEquals('Any content you want', new Html(null, [], 'Any content you want'));
    }

    public function testEmptyTagWithAttributesAndContentEqualsContent()
    {
        self::assertEquals('Any content you want', new Html(null, ['src' => 'example.com/test'], 'Any content you want'));
    }

    public function testTagWithContent()
    {
        self::assertEquals('<b style="color:white">Any content you want</b>', new Html('b', ['style' => 'color:white'], 'Any content you want'));
    }
}
