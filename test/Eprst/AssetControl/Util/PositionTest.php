<?php

namespace Test\Eprst\AssetControl\Util;

use Eprst\AssetControl\Util\Position;

class PositionTest extends \PHPUnit_Framework_TestCase
{
    public function testGetters()
    {
        $p = new Position(10, 20);

        self::assertEquals(10, $p->getStartOffset());
        self::assertEquals(20, $p->getLength());
    }

    public function testExceptionOnNegativeStartOffset()
    {
        self::setExpectedException('\Eprst\AssetControl\Aom\Exception\InvalidPositionException',
                                   'Start offset cannot be negative');

        new Position(-10, 1);
    }

    public function testExceptionOnZeroLength()
    {
        self::setExpectedException('\Eprst\AssetControl\Aom\Exception\InvalidPositionException',
                                   'Length cannot be zero or negative');

        new Position(0, 0);
    }

    public function testExceptionOnNegativeLength()
    {
        self::setExpectedException('\Eprst\AssetControl\Aom\Exception\InvalidPositionException',
                                   'Length cannot be zero or negative');

        new Position(0, -10);
    }

    public function testExceptionOnInvalidTypes()
    {
        self::setExpectedException('\Eprst\AssetControl\Aom\Exception\InvalidPositionException',
                                   'Length cannot be zero or negative');

        new Position('hello world', 'foo bar');
    }
}
