<?php


namespace Test\Eprst\AssetControl\Aom;


use Eprst\AssetControl\Aom\Asset\AssetReference;
use Eprst\AssetControl\Aom\Chunk;
use Eprst\AssetControl\Util\Path;
use Eprst\AssetControl\Util\Position;

class ChunkTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var Chunk
     */
    private $chunk;

    protected function setUp()
    {
        $asset1 = new AssetReference(new Path('/var/www/script.js'), new Path('/js/script.js'));
        $asset2 = new AssetReference(new Path('/var/www/script.js'), new Path('/css/script.css'));

        $this->chunk = new Chunk(new Position(0, 1), [$asset1, $asset2]);
    }

    public function testGetters()
    {
        $asset1 = new AssetReference(new Path('/var/www/script.js'), new Path('/js/script.js'));
        $asset2 = new AssetReference(new Path('/var/www/script.js'), new Path('/css/script.css'));

        $this->chunk = new Chunk(new Position(0, 1), [$asset1, $asset2]);

        self::assertEquals([$asset1, $asset2], $this->chunk->getAssets());
        self::assertEquals(new Position(0, 1), $this->chunk->getPosition());
    }

    public function testTypeOfChunkIsTypeOfFirstAsset()
    {
        self::assertEquals($this->chunk->getAssets()[0]->getType(), $this->chunk->getType());
    }

    public function testCastChunkToString()
    {
        $expected = <<<STR
<script type="text/javascript" src="/js/script.js"></script>
<link rel="stylesheet" type="text/css" href="/css/script.css"></link>
STR;
        self::assertEquals($expected, (string) $this->chunk);
    }
}
