<?php


namespace Test\Eprst\AssetControl\Aom\Asset;


use Eprst\AssetControl\Aom\Asset\InlineAsset;
use Eprst\AssetControl\Aom\Asset\Type\StylesheetType;

class InlineAssetTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var callable
     */
    private static $emptyCb;

    public static function setUpBeforeClass()
    {
        self::$emptyCb = function(){};
    }


    public function testContentGetter()
    {
        $ia1 = new InlineAsset('Hello, world', new StylesheetType(), self::$emptyCb);

        self::assertEquals('Hello, world', $ia1->getContent());
    }
    public function testVirtualPath()
    {
        $ia1 = new InlineAsset('Hello, world', new StylesheetType(), self::$emptyCb);

        self::assertStringEndsWith('.css', $ia1->getVirtualPath());
        self::assertGreaterThan(4, strlen($ia1->getVirtualPath()));
    }

    public function testVirtualPathSameForSameContent()
    {
        $ia1 = new InlineAsset('Hello, world', new StylesheetType(), self::$emptyCb);
        $ia2 = new InlineAsset('Hello, world', new StylesheetType(), self::$emptyCb);
        $ia3 = new InlineAsset('Hello, great world', new StylesheetType(), self::$emptyCb);

        self::assertEquals($ia1->getVirtualPath(), $ia2->getVirtualPath());

        self::assertNotEquals($ia1->getVirtualPath(), $ia3->getVirtualPath());
        self::assertNotEquals($ia2->getVirtualPath(), $ia3->getVirtualPath());
    }

    public function testPutContentOnDemand()
    {
        $ia1 = new InlineAsset('Hello, great world', new StylesheetType(), self::$emptyCb);

        $real = implode('', [sys_get_temp_dir(), DIRECTORY_SEPARATOR, $ia1->getVirtualPath()]);

        self::assertFileNotExists($real);

        self::assertStringEndsWith((string) $ia1->getVirtualPath(), (string) $ia1->getRealPath());

        self::assertEquals('Hello, great world', file_get_contents((string) $ia1->getRealPath()));

        self::assertFileExists($real);

        unlink($real);
    }

    public function testCallbackIsCalledWhenPuttingContent()
    {
        $isCalled = false;
        $ia1 = new InlineAsset('Hello, great world', new StylesheetType(), function()use(&$isCalled){
            $isCalled = true;
        });

        $real = $ia1->getRealPath();

        unlink((string)$real);

        self::assertTrue($isCalled);
    }

    public function testPutContentOnlyOnce()
    {
        $ia1 = new InlineAsset('Hello, great world', new StylesheetType(), self::$emptyCb);

        $real = $ia1->getRealPath();

        $fh = fopen($real, 'r+');
        self::assertNotEmpty($fh, "TestSuite alert: unable to open {$real} for reading");
        self::assertTrue(flock($fh, LOCK_EX), "TestSuite alert: unable to flock {$real}");

        $caught = null;

        try {
            $real = $ia1->getRealPath();
        } catch (\Exception $exc) {
            $caught = $exc;
        } finally {
            flock($fh, LOCK_UN);
            fclose($fh);
            unlink($real);
        }

        if ($caught) {
            throw $caught;
        }
    }

    public function testCastToHtml()
    {
        $type = new StylesheetType();
        $ia1 = new InlineAsset('Hello, world', $type, self::$emptyCb);

        self::assertEquals($type->toHtml($ia1), $ia1->toHtml());
    }
}
