<?php

namespace Eprst\AssetControl\Ui;

use Eprst\AssetControl\Ui\Command\CompileCommand;
use Eprst\AssetControl\Ui\Command\InspectCommand;
use \Symfony\Component\Console\Application as SymfonyConsoleApplication;


class Application extends SymfonyConsoleApplication
{
    public function __construct($name = 'UNKNOWN', $version = 'UNKNOWN')
    {
        parent::__construct('AssetControl', '1.0');

        $this->add(new InspectCommand());
        $this->add(new CompileCommand());
    }
}