<?php


namespace Eprst\AssetControl\Aom;

use Eprst\AssetControl\Aom\Asset\Asset;
use Eprst\AssetControl\Aom\Asset\Type\Type;
use Eprst\AssetControl\Util\Position;

class Chunk
{
    /**
     * @var Position
     */
    private $position;

    /**
     * @var Asset[]
     */
    private $assets;

    /**
     * @param Position $position
     * @param Asset[]  $assets
     */
    public function __construct(Position $position, $assets)
    {
        $this->position = $position;
        $this->assets   = $assets;
    }

    /**
     * @return Position
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @return Asset[]
     */
    public function getAssets()
    {
        return $this->assets;
    }


    /**
     * @return Asset[][]
     */
    public function getAssetsByType()
    {
        $index = [];

        foreach ($this->assets as $a) {
            $index[get_class($a->getType())][] = $a;
        }

        return $index;
    }

    /**
     * @return Type
     */
    public function getType()
    {
        return $this->assets[0]->getType();
    }

    public function toHtmlArray()
    {
        return array_map(function($a){
            /** @var Asset $a */
            return $a->getType()->toHtml($a);
        }, $this->assets);
    }

    public function toHtmlString($delim = "")
    {
        return implode($delim, $this->toHtmlArray());
    }

    function __toString()
    {
        return $this->toHtmlString("\n");
    }
}