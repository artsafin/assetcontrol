<?php


namespace Eprst\AssetControl\Aom;

use Eprst\AssetControl\Util\Path;

interface FileParser
{
    public function parse(RawFile $file, Path $assetRoot);
}