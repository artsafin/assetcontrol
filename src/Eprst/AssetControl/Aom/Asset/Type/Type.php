<?php

namespace Eprst\AssetControl\Aom\Asset\Type;

use Eprst\AssetControl\Aom\Asset\Asset;
use Eprst\AssetControl\Util\Html;

interface Type
{
    /**
     * @param bool $prefixWithDot
     *
     * @return string
     */
    public function getExtension($prefixWithDot = false);

    /**
     * @param Asset $asset
     *
     * @return Html
     */
    public function toHtml(Asset $asset);

    public function equals(Type $type);
}