<?php


namespace Eprst\AssetControl\Aom\Exception;

/**
 * Class InvalidPositionException
 *
 * @codeCoverageIgnore
 */
class InvalidPositionException extends \Exception
{

}