<?php


namespace Test\Eprst\AssetControl\Aom\Asset;


use Eprst\AssetControl\Aom\Asset\TransformedAsset;
use Eprst\AssetControl\Aom\Asset\Type\JavascriptType;
use Eprst\AssetControl\Util\Path;

/**
 * @property Path resultDir
 * @property Path virtualPathPrefix
 */
class TransformedAssetTest extends \PHPUnit_Framework_TestCase
{
    protected function setUp()
    {
        $this->resultDir = new Path(sys_get_temp_dir(), true);
        $this->virtualPathPrefix = new Path('virtual');
    }


    public function testRealizationOfRealPath()
    {
        $a = new TransformedAsset(new JavascriptType(), 'test content', $this->resultDir, $this->virtualPathPrefix);

        $path = $a->getRealPath();

        self::assertEquals('test content', file_get_contents($path));

        unlink($path);
    }
}
