<?php

namespace Eprst\AssetControl\Ui\Input;

use Eprst\AssetControl\App\Build\FileLocator;
use Eprst\AssetControl\App\Parameters;
use Eprst\AssetControl\Util\Path;
use Symfony\Component\Console\Input\InputInterface;

class InputReader implements Parameters
{
    /**
     * @var InputInterface
     */
    private $input;

    /**
     * @var Path
     */
    private $cwd;

    /**
     * @var string[]
     */
    private $expandedPaths;

    /**
     * @var Path
     */
    private $assetRoot;

    public function __construct(InputInterface $input)
    {
        $this->input = $input;

        $this->cwd = new Path(getcwd(), true);
    }

    public function assetRoot()
    {
        if (!$this->assetRoot) {
            if ($assetRoot = $this->input->getOption(self::ASSET_ROOT)) {
                $assetRoot = new Path($assetRoot, true);
            } else if (count($this->targets())) {
                $assetRoot = new Path($this->targets()[0]);
                if (!$assetRoot->isDirectory()) {
                    $assetRoot = $assetRoot->getDeepestDirectory();
                }
            } else {
                $assetRoot = $this->cwd;
            }

            $this->assetRoot = $assetRoot->toAbsolute($this->cwd);
        }

        return $this->assetRoot;
    }

    public function compileDir()
    {
        return (new Path($this->input->getOption(self::COMPILE_DIR), true))->toAbsolute($this->assetRoot());
    }

    public function targets()
    {
        if (!$this->expandedPaths) {
            $targets = $this->input->getArgument(self::TARGETS);
            $this->expandedPaths = FileLocator::expandPaths($targets, $this->cwd);
        }

        return $this->expandedPaths;
    }

    public function compileVirtualPath()
    {
        return new Path($this->input->getOption(self::COMPILE_VIRTUAL_PATH) ? : '', true);
    }

    /**
     * @return bool
     */
    public function dryRun()
    {
        return (bool) $this->input->getOption(self::DRY_RUN);
    }

    /**
     * @return string
     */
    public function transformTool()
    {
        $tool = $this->input->getOption(self::TRANSFORM_TOOL);

        if ($tool === 'cmd' && !$this->transformToolCmd()) {
            throw new \RuntimeException("You must specify --transform-tool-cmd option for \"cmd\" tool");
        }

        return $tool;
    }

    private function interpolateByOptions($string)
    {
        if (empty($string)) {
            return '';
        }

        $string = preg_replace_callback('|(:[^:]+:)|', function($matches){
            $optName = trim($matches[1], ':');
            if ($this->input->hasOption($optName)) {
                return $this->input->getOption($optName);
            } else {
                return $matches[1];
            }
        }, $string);

        return $string;
    }

    /**
     * @return string
     */
    public function transformToolCmd()
    {
        $cmd = $this->input->getOption(self::TRANSFORM_TOOL_CMD);

        return $this->interpolateByOptions($cmd);
    }
}