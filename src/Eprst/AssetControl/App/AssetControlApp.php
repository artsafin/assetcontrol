<?php


namespace Eprst\AssetControl\App;

use Eprst\AssetControl\Aom\AssetObjectModel;
use Eprst\AssetControl\Aom\Chunk;
use Eprst\AssetControl\Aom\FileParser;
use Eprst\AssetControl\App\Transform\Factory;
use Eprst\AssetControl\Util\FilesystemFile;

class AssetControlApp
{
    /**
     * @var FileParser
     */
    private $parser;

    public function __construct(FileParser $parser)
    {
        $this->parser = $parser;
    }

    /**
     * @param Parameters $conf
     *
     * @return AssetObjectModel
     */
    public function buildAssetObjectModel(Parameters $conf)
    {
        $result = [];

        foreach ($conf->targets() as $file) {
            $file = new FilesystemFile($file);

            $result[] = $this->parser->parse($file, $conf->assetRoot());
        }

        return new AssetObjectModel($result);
    }

    public function transformModel(Parameters $conf, CleanupManager $cleanup)
    {
        $model = $this->buildAssetObjectModel($conf);

        $adapter = Factory::createByAlias($conf->transformTool(), $conf->transformToolCmd(), $cleanup);

        foreach ($model->getAssetFiles() as $file) {
            foreach ($file->getChunks() as $chunk) {
                $replaceAssets = $adapter->compile($chunk, $conf);
                $replaceChunk = new Chunk($chunk->getPosition(), $replaceAssets);

                $path = $conf->dryRun() ? $file->getPath()->suffixWith('.ac') : $file->getPath();
                $file->replace($chunk, $replaceChunk, $path);
            }
        }

        return $model;
    }
}