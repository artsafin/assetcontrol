<?php


namespace Test\Eprst\AssetControl\Aom\Asset;


use Eprst\AssetControl\Aom\Asset\AssetReference;
use Eprst\AssetControl\Aom\Asset\Type\JavascriptType;
use Eprst\AssetControl\Aom\Asset\Type\StylesheetType;
use Eprst\AssetControl\Util\Path;

class AssetReferenceTest extends \PHPUnit_Framework_TestCase
{
    public function testPathGetters()
    {
        $ar = new AssetReference(new Path('/foo/bar/baz/qux.js'), new Path('baz/qux.js'));

        self::assertEquals('/foo/bar/baz/qux.js', (string) $ar->getRealPath());
        self::assertEquals('baz/qux.js', (string) $ar->getVirtualPath());
        self::assertEquals(new JavascriptType(), $ar->getType());
    }

    public function testTypeGuess()
    {
        $js = new AssetReference(new Path('/foo/bar/baz/qux.js'), new Path('baz/qux.js'));
        $css = new AssetReference(new Path('/foo/bar/baz/qux.css'), new Path('baz/qux.css'));

        self::assertEquals(new StylesheetType(), $css->getType());
        self::assertEquals(new JavascriptType(), $js->getType());

        self::setExpectedException('\Eprst\AssetControl\Aom\Exception\UnknownAssetTypeException');
        new AssetReference(new Path('qwe'), new Path('qwe'));
    }

    public function testContent()
    {
        $cssRealPath = sprintf('%s%stest_%s.css', sys_get_temp_dir(), DIRECTORY_SEPARATOR, uniqid());
        file_put_contents($cssRealPath, 'Hello, css');
        self::assertFileExists($cssRealPath, "TestSuite alert: failed to write {$cssRealPath} in test environment");

        $css = new AssetReference(new Path($cssRealPath), new Path('test.css'));

        self::assertEquals('Hello, css', $css->getContent());

        unlink($cssRealPath);
    }

    public function testCastToHtml()
    {
        $js  = new AssetReference(new Path('/foo/bar/baz/qux.js'), new Path('baz/qux.js'));
        $css = new AssetReference(new Path('/foo/bar/baz/qux.css'), new Path('baz/qux.css'));

        self::assertEquals('<script type="text/javascript" src="baz/qux.js"></script>', (string) $js->toHtml());
        self::assertEquals('<link rel="stylesheet" type="text/css" href="baz/qux.css"></link>', (string) $css->toHtml());
    }
}
