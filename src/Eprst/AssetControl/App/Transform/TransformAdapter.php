<?php


namespace Eprst\AssetControl\App\Transform;


use Eprst\AssetControl\Aom\Asset\Asset;
use Eprst\AssetControl\Aom\Chunk;
use Eprst\AssetControl\App\CleanupManager;
use Eprst\AssetControl\App\Parameters;

interface TransformAdapter
{
    /**
     * @param string         $cmd
     * @param CleanupManager $cleanupManager
     */
    public function __construct($cmd, CleanupManager $cleanupManager);

    /**
     * @param Chunk      $chunk
     * @param Parameters $conf
     *
     * @return Asset[]
     */
    public function compile(Chunk $chunk, Parameters $conf);
}