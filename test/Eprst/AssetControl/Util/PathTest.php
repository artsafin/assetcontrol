<?php

namespace Test\Eprst\AssetControl\Util;

use Eprst\AssetControl\Util\Path;

class PathTest extends \PHPUnit_Framework_TestCase
{
    public function testLocality()
    {
        self::assertTrue((new Path('/home/www-data/test.js'))->isLocal());
        self::assertTrue((new Path('C:\Users\www-data\test.js'))->isLocal());
        self::assertTrue((new Path('file:///C:/Users/www-data/test.js'))->isLocal());

        self::assertFalse((new Path('http://example.com/test.js'))->isLocal());
        self::assertFalse((new Path('ssh://example.com/test.js'))->isLocal());
    }

    public function testRelative()
    {
        self::assertFalse((new Path('/home/www-data/test.js'))->isRelative());
        self::assertFalse((new Path('C:\Users\test.js'))->isRelative());
        self::assertFalse((new Path('C:/Users/test.js'))->isRelative());
        self::assertFalse((new Path('/Users/test.js'))->isRelative());
        self::assertFalse((new Path('http://example.com/test.js'))->isRelative());

        self::assertTrue((new Path('Users/test.js'))->isRelative());
        self::assertTrue((new Path('../js/test.js'))->isRelative());
        self::assertTrue((new Path('..\js\test.js'))->isRelative());
    }

    public function testCastToString()
    {
        self::assertEquals('hello world', new Path('hello world'));
        self::assertEquals('/Users/test.js', new Path('/Users/test.js'));
    }

    public function testExtension()
    {
        self::assertEquals('',     (new Path('/home/www-data/test'))->getExtension());
        self::assertEquals('',     (new Path('http://example.com/test.'))->getExtension());
        self::assertEquals('',     (new Path('http://example.com/test..'))->getExtension());
        self::assertEquals('a',    (new Path('http://example.com/test..a'))->getExtension());
        self::assertEquals('js',   (new Path('/home/www-data/test.js'))->getExtension());
        self::assertEquals('html', (new Path('/home/www-data/test.js.css.html'))->getExtension());
        self::assertEquals('css',  (new Path('/home/www.data/test.js.css'))->getExtension());
        self::assertEquals('js',   (new Path('http://example.com/test.js'))->getExtension());
    }
}
