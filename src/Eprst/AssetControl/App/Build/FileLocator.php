<?php


namespace Eprst\AssetControl\App\Build;

use Eprst\AssetControl\Util\Path;
use SplFileInfo;

class FileLocator
{
    private static function expandSimpleGlob($paths)
    {
        $acc = [];

        foreach ($paths as $item) {
            if (strpos($item, '**') !== false) {
                continue;
            }

            if (strpos($item, '*') !== false || strpos($item, '?') !== false) {
                $glob = glob($item, GLOB_BRACE);
                if ($glob !== false) {
                    $acc = array_merge($acc, $glob);
                }
            } else {
                $acc[] = $item;
            }
        }

        return $acc;
    }

    private static function getDoubleStarRoots($paths)
    {
        $rootToPattern = [];

        foreach ($paths as $p) {
            if (strpos($p, '**') === false) {
                continue;
            }

            list($root) = explode('**', $p, 2);

            $sameRootOrigin = false;

            foreach (array_keys($rootToPattern) as $_r) {
                if (strpos($root, $_r) === 0) { // Existing root _r is more generic
                    $rootToPattern[$_r][] = $p;
                    $sameRootOrigin       = true;
                    break;
                } else if (strpos($_r, $root) === 0) { // New root root is more generic than existing
                    $rootToPattern[$root]   = $rootToPattern[$_r];
                    $rootToPattern[$root][] = $p;
                    unset($rootToPattern[$_r]);
                    $sameRootOrigin = true;
                    break;
                }
            }

            if (!$sameRootOrigin) {
                $rootToPattern[$root][] = $p;
            }
        }

        return $rootToPattern;
    }

    private static function isPathMatchesDoubleStarPattern($path, $pattern)
    {
        if (defined('PHP_WINDOWS_VERSION_BUILD')) {
            $pattern = str_replace('\\', '/', $pattern);
            $path = str_replace('\\', '/', $path);
        }

        $re = str_replace('\*\*', '.*', preg_quote($pattern, '|'));

        return preg_match("|{$re}|", $path);
    }

    private static function expandDoubleStar($paths)
    {
        $result = [];

        $roots = self::getDoubleStarRoots($paths);

        foreach ($roots as $root => $patterns) {
            $rdi = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($root));
            /** @var SplFileInfo $fileInfo */
            foreach ($rdi as $path => $fileInfo) {
                if (!$fileInfo->isFile() || $rdi->isDot()) {
                    continue;
                }
                foreach ($patterns as $pattern) {
                    if (self::isPathMatchesDoubleStarPattern($fileInfo->getPathname(), $pattern)) {
                        $result[] = $fileInfo->getPathname();
                    }
                }
            }
        }

        return $result;
    }

    private static function absolutizePaths($paths, Path $relativePathRoot)
    {
        return array_map(function ($item) use ($relativePathRoot) {
            return (string)(new Path($item))->toAbsolute($relativePathRoot);
        },
            $paths);
    }

    public static function expandPaths($paths, Path $relativePathRoot = null, $recurse = true)
    {
        $expandedPaths = self::expandSimpleGlob($paths);
        $expandedPaths = $relativePathRoot ? self::absolutizePaths($expandedPaths, $relativePathRoot) : $expandedPaths;

        $expandedDirs = array_filter($expandedPaths, 'is_dir');
        $expandedFiles = array_diff($expandedPaths, $expandedDirs);

        if ($recurse) {
            $expandedDirs = array_map(function($item){
                return rtrim($item, '/\\') . '/**';
            }, $expandedDirs);
            $doubleStarPaths = array_merge($paths, $expandedDirs);
        } else {
            $doubleStarPaths = $paths;
        }
        $doubleStarPaths = $relativePathRoot ? self::absolutizePaths($doubleStarPaths, $relativePathRoot) : $doubleStarPaths;
        $doubleStarFiles = self::expandDoubleStar($doubleStarPaths);

        return array_merge($expandedFiles, $doubleStarFiles);
    }
}