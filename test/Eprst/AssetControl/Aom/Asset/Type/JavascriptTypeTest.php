<?php


namespace Test\Eprst\AssetControl\Aom\Asset\Type;


use Eprst\AssetControl\Aom\Asset\AssetReference;
use Eprst\AssetControl\Aom\Asset\Type\JavascriptType;
use Eprst\AssetControl\Aom\Asset\Type\StylesheetType;
use Eprst\AssetControl\Util\Path;

class JavascriptTypeTest extends \PHPUnit_Framework_TestCase
{
    public function testToHtml()
    {
        $a = new AssetReference(new Path('/home/test.js'), new Path('test.js'));
        $t = new JavascriptType();

        self::assertEquals('<script type="text/javascript" src="test.js"></script>', (string) $t->toHtml($a));
    }

    public function testGetExtension()
    {
        $t = new JavascriptType();

        self::assertEquals('js', $t->getExtension());
        self::assertEquals('.js', $t->getExtension(true));
    }

    public function testEquals()
    {
        $t = new JavascriptType();

        self::assertFalse($t->equals(new StylesheetType()));
        self::assertTrue($t->equals(new JavascriptType()));
    }
}
