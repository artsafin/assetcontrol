<?php


namespace Eprst\AssetControl\App;


use Symfony\Component\Console\Event\ConsoleTerminateEvent;
use Symfony\Component\Console\Output\OutputInterface;

class CleanupManager
{
    private $files = [];
    /**
     * @var OutputInterface
     */
    private $output;

    function __construct(OutputInterface $output = null)
    {
        $this->output = $output;
    }

    public function removeFile($fileName)
    {
        $this->files[] = $fileName;
    }

    public function cleanup()
    {
        if ($this->isCliVerbose() && $this->files) {
            $this->output->writeln(['', 'Cleaning up...']);
        }

        foreach ($this->files as $f) {
            $this->deleteFile($f);
        }
    }

    private function isCliVerbose()
    {
        return $this->output && $this->output->getVerbosity() == OutputInterface::VERBOSITY_VERBOSE;
    }

    public function onCliTerminate(ConsoleTerminateEvent $terminateEvent)
    {
        $this->cleanup();
    }

    private function deleteFile($file)
    {
        if (!$this->isCliVerbose()) {
            @unlink((string) $file);
        } else {
            if (unlink((string) $file)) {
                $this->output->writeln("Deleted {$file}");
            } else {
                $this->output->writeln("Failed to delete {$file}");
            }
        }
    }
}