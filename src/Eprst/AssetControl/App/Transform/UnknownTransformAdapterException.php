<?php


namespace Eprst\AssetControl\App\Transform;


use Exception;

class UnknownTransformAdapterException extends \Exception
{
    public function __construct($message = "", $code = 0, Exception $previous = null)
    {
        parent::__construct("Error initializing transform adapter" . ($message?": {$message}":''), $code, $previous);
    }

}