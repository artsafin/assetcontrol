<?php


namespace Eprst\AssetControl\Aom\Asset;

use Eprst\AssetControl\Aom\Asset\Type\Type;
use Eprst\AssetControl\Aom\Asset\Type\TypeFactory;
use Eprst\AssetControl\Util\Html;
use Eprst\AssetControl\Util\Path;

class AssetReference implements Asset
{
    /**
     * @var Path
     */
    private $virtualPath;
    /**
     * @var Path
     */
    private $realPath;
    /**
     * @var Type
     */
    private $type;

    public function __construct(Path $realPath, Path $virtualPath)
    {
        $this->virtualPath = $virtualPath;
        $this->realPath    = $realPath;
        $this->type        = TypeFactory::createByExtension($virtualPath);
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return file_get_contents($this->getRealPath());
    }

    /**
     * @return Path
     */
    public function getVirtualPath()
    {
        return $this->virtualPath;
    }

    /**
     * @return Path
     */
    public function getRealPath()
    {
        return $this->realPath;
    }

    /**
     * @return Type
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return Html
     */
    public function toHtml()
    {
        return $this->type->toHtml($this);
    }
}