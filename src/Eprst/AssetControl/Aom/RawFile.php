<?php

namespace Eprst\AssetControl\Aom;

use Eprst\AssetControl\Util\Path;
use Eprst\AssetControl\Util\Position;

interface RawFile
{
    /**
     * @return string
     */
    public function loadContent();

    /**
     * @return Path
     */
    public function getPath();

    public function replace(Position $position, $string, Path $path = null);
}