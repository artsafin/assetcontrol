<?php


namespace Eprst\AssetControl\Aom\Asset\Type;


use Eprst\AssetControl\Aom\Asset\Asset;
use Eprst\AssetControl\Util\Html;

class StylesheetType implements Type
{
    /**
     * @param Asset $asset
     *
     * @return Html
     */
    public function toHtml(Asset $asset)
    {
        return Html::link()->_rel('stylesheet')->_type('text/css')->_href($asset->getVirtualPath());
    }

    public function equals(Type $type)
    {
        return $type instanceof self;
    }

    /**
     * @param bool $prefixWithDot
     *
     * @return string
     */
    public function getExtension($prefixWithDot = false)
    {
        return ($prefixWithDot ? '.' : '') . 'css';
    }
}