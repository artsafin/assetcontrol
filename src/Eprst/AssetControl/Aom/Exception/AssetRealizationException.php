<?php


namespace Eprst\AssetControl\Aom\Exception;
use Exception;

/**
 * Class AssetRealizationException
 *
 * @codeCoverageIgnore
 */
class AssetRealizationException extends \Exception
{
    public function __construct($message = "", $code = 0, Exception $previous = null)
    {
        parent::__construct("Unable to cache inline asset content" . ($message?": {$message}":''), $code, $previous);
    }

}