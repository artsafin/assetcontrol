<?php


namespace Eprst\AssetControl\Util;


use Eprst\AssetControl\Aom\Exception\InvalidPositionException;

class Position
{
    private $startOffset;

    private $length;

    public function __construct($startOffset, $length)
    {
        $this->startOffset = (int) $startOffset;
        $this->length      = (int) $length;

        if ($this->length <= 0) {
            throw new InvalidPositionException("Length cannot be zero or negative");
        }

        if ($this->startOffset < 0) {
            throw new InvalidPositionException("Start offset cannot be negative");
        }
    }

    /**
     * @return int
     */
    public function getStartOffset()
    {
        return $this->startOffset;
    }

    /**
     * @return int
     */
    public function getLength()
    {
        return $this->length;
    }

    function __toString()
    {
        return sprintf('(%s:%s)', $this->startOffset, $this->startOffset + $this->length);
    }


}