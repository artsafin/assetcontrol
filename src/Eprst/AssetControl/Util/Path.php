<?php
namespace Eprst\AssetControl\Util;

class Path
{
    const S = '/';
    const TRIMS = '/\\';

    /**
     * @var string
     */
    private $path;

    /**
     * @var bool
     */
    private $isAbsolute;

    /**
     * @var bool
     */
    private $isLocal;

    /**
     * @var string
     */
    private $parsedPath;

    /**
     * @var string
     */
    private $extension;

    /**
     * @var bool
     */
    private $isDir;

    private static function isWindows()
    {
        return defined('PHP_WINDOWS_VERSION_BUILD');
    }

    /**
     * @param Path|string $path
     * @param string $isDir
     */
    public function __construct($path, $isDir = null)
    {
        $path = (string) $path;

        if (self::isWindows()) {
            $path = str_replace('\\', self::S, $path);
        }

        $this->path       = $path;
        $this->parsedPath = parse_url($this->path);

        $this->isLocal = !isset($this->parsedPath['host']);
        if ($this->isLocal) {
            $this->isAbsolute = (strlen($path) >= 1 && $this->path[0] == self::S)
                                || self::isWindows() && (strlen($path) >= 2 && $path[1] == ':');
        } else {
            $this->isAbsolute = true;
        }

        $this->extension = $this->findExtension();

        $this->isDir = ($isDir === null && strlen($this->path) > 0 && $this->path[strlen($this->path) - 1] == self::S) || $isDir;
    }

    private function findExtension()
    {
        if (($pos = strrpos($this->path, '.')) !== false
            && $pos < (strlen($this->path) - 1)
        ) {
            return substr($this->path, $pos + 1);
        }

        return '';
    }

    public function isRelative()
    {
        return !$this->isAbsolute;
    }

    public function isLocal()
    {
        return $this->isLocal;
    }

    public function isDirectory()
    {
        return $this->isDir;
    }

    public function getExtension()
    {
        return $this->extension;
    }

    public function __toString()
    {
        return rtrim($this->path, self::S) . ($this->isDir ? self::S : '');
    }

    public function toNativeString()
    {
        $result = (string) $this;

        if (self::isWindows()) {
            return str_replace('/', '\\', $result);
        }

        return $result;
    }

    /**
     * @param int $offset
     * @param int $length
     *
     * @return Path
     */
    private function getSlice($offset, $length)
    {
        $part = array_slice(explode(self::S, $this->path), $offset, $length);

        return new Path(count($part) ? implode(self::S, $part) : '');
    }

    /**
     * @param Path $path
     *
     * @return Path
     */
    public function prefixWith(Path $path)
    {
        $left  = rtrim((string) $path, self::TRIMS);
        $right = ltrim((string) $this, self::TRIMS);

        return new Path($left . self::S . $right);
    }

    public function suffixWith($strOrPath)
    {
        return new Path($this->path . ((string) $strOrPath), $this->isDirectory());
    }

    public function toAbsolute(Path $root)
    {
        if ($this->isAbsolute) {
            return $this;
        }

        return $this->prefixWith($root->getDeepestDirectory());
    }

    public function getDiff(Path $other)
    {
        $otherStr = rtrim((string)$other, self::TRIMS);

        if ($otherStr && strpos($this->path, $otherStr) === 0) {
            return new Path(ltrim(substr($this->path, strlen($otherStr)), self::TRIMS));
        } else {
            return new Path('');
        }
    }

    /**
     * @return Path
     */
    public function getDeepestDirectory()
    {
        return $this->isDir ? $this : $this->getSlice(0, -1);
    }
}