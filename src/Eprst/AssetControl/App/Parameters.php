<?php


namespace Eprst\AssetControl\App;

use Eprst\AssetControl\Util\Path;

interface Parameters
{
    const ASSET_ROOT = 'asset-root';
    const COMPILE_DIR = 'compile-dir';
    const COMPILE_VIRTUAL_PATH = 'compile-path';
    const TARGETS = 'target';
    const DRY_RUN = 'dry-run';
    const TRANSFORM_TOOL = 'transform-tool';
    const TRANSFORM_TOOL_CMD = 'transform-tool-cmd';

    /**
     * @return Path
     */
    public function assetRoot();

    /**
     * @return Path
     */
    public function compileDir();

    /**
     * @return string[]
     */
    public function targets();

    /**
     * @return Path
     */
    public function compileVirtualPath();

    /**
     * @return bool
     */
    public function dryRun();

    /**
     * @return string
     */
    public function transformTool();

    /**
     * @return string
     */
    public function transformToolCmd();
}