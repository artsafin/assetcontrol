<?php
namespace Eprst\AssetControl\Ui\Command;

use Eprst\AssetControl\Aom\AssetObjectModel;
use Eprst\AssetControl\Aom\Chunk;
use Eprst\AssetControl\App\AssetControlApp;
use Eprst\AssetControl\App\Build\ParserImpl;
use Eprst\AssetControl\App\CleanupManager;
use Eprst\AssetControl\Ui\Input\Input;
use Eprst\AssetControl\Ui\Input\InputReader;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CompileCommand extends Command
{
    protected function configure()
    {
        $this->setName('compile')
             ->setDescription('Compile assets')
             ->setHelp('Compile assets');

        Input::requireInput()
            ->assetRoot()
            ->compileDir()
            ->compileVirtualPath()
            ->dryRun()
            ->transformTool()
            ->transformToolCmd()
            ->targets()
            ->mergeInto($this->getDefinition());
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $inputReader = new InputReader($input);

        $cleanup = new CleanupManager($output);
        $app = new AssetControlApp(new ParserImpl('asset-compile', $cleanup));

        $model = $app->transformModel($inputReader, $cleanup);

        $this->renderReplacements($model, $output);

        $cleanup->cleanup();
    }

    /**
     * @param AssetObjectModel $model
     * @param OutputInterface  $output
     */
    private function renderReplacements(AssetObjectModel $model, OutputInterface $output)
    {
        $level = function ($lvl) {
            return function () use ($lvl) {
                return str_repeat(' ', $lvl * 4);
            };
        };

        foreach ($model->getAssetFiles() as $file) {
            if (!$file->getReplacements()) {
                continue;
            }

            $l = $level(0);
            $output->write($l() . "File {$file->getPath()}: ");

            $output->writeln($l() . count($file->getReplacements()) . ' chunk');

            /** @var Chunk $old */
            /** @var Chunk $new */
            foreach ($file->getReplacements() as list($old, $new)) {
                $l = $level(1);
                $output->writeln($l() . sprintf('%s chunk %s:',
                                                $old->getType()->getExtension(),
                                                $old->getPosition()));

                foreach ($new->getAssets() as $asset) {
                    $l = $level(2);
                    $output->writeln($l() . sprintf('%s -> %s', $asset->getVirtualPath(), $asset->getRealPath()));
                }
            }
        }
    }
}