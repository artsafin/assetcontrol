<?php


namespace Eprst\AssetControl\Aom\Asset\Type;


use Eprst\AssetControl\Aom\Asset\Asset;
use Eprst\AssetControl\Util\Html;

class JavascriptType implements Type
{
    /**
     * @param Asset $asset
     *
     * @return Html
     */
    public function toHtml(Asset $asset)
    {
        return Html::script()->_type('text/javascript')->_src($asset->getVirtualPath());
    }

    public function equals(Type $type)
    {
        return $type instanceof self;
    }

    /**
     * @param bool $prefixWithDot
     *
     * @return string
     */
    public function getExtension($prefixWithDot = false)
    {
        return ($prefixWithDot ? '.' : '') . 'js';
    }
}