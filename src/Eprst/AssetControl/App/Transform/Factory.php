<?php


namespace Eprst\AssetControl\App\Transform;

use Eprst\AssetControl\App\CleanupManager;

class Factory
{
    /**
     * @param string         $alias
     * @param string         $cmdString
     * @param CleanupManager $cleanup
     *
     * @return TransformAdapter
     * @throws UnknownTransformAdapterException
     */
    public static function createByAlias($alias, $cmdString, CleanupManager $cleanup)
    {
        $className = sprintf('%s\\%s\\%sAdapter', __NAMESPACE__, ucfirst($alias), ucfirst($alias));
        if (!class_exists($className)) {
            throw new UnknownTransformAdapterException("{$alias} not found");
        }

        $o = new $className($cmdString, $cleanup);
        if (!$o instanceof TransformAdapter) {
            throw new UnknownTransformAdapterException("{$alias} is invalid");
        }

        return $o;
    }
}