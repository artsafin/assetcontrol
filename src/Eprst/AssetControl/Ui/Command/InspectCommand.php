<?php
namespace Eprst\AssetControl\Ui\Command;

use Eprst\AssetControl\Aom\Asset\InlineAsset;
use Eprst\AssetControl\Aom\AssetObjectModel;
use Eprst\AssetControl\App\AssetControlApp;
use Eprst\AssetControl\App\Build\ParserImpl;
use Eprst\AssetControl\App\CleanupManager;
use Eprst\AssetControl\Ui\Input\Input;
use Eprst\AssetControl\Ui\Input\InputReader;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class InspectCommand extends Command
{
    protected function configure()
    {
        $this->setName('inspect')
             ->setDescription('Inspect assets')
             ->setHelp('Inspect assets');

        Input::requireInput()
             ->assetRoot()
             ->targets()
             ->mergeInto($this->getDefinition());
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $inputReader = new InputReader($input);
        $cleanupManager = new CleanupManager($output);

        $app = new AssetControlApp(new ParserImpl('asset-compile', $cleanupManager));

        $model = $app->buildAssetObjectModel($inputReader);

        $this->renderModel($model, $output);

        $cleanupManager->cleanup();
    }

    /**
     * @param AssetObjectModel $model
     * @param OutputInterface  $output
     */
    private function renderModel(AssetObjectModel $model, OutputInterface $output)
    {
        $level = function ($lvl) {
            return function () use ($lvl) {
                return str_repeat(' ', $lvl * 4);
            };
        };

        foreach ($model->getAssetFiles() as $file) {
            $l = $level(0);
            $output->write($l() . "File {$file->getPath()}: ");

            if (!$file->getChunks()) {
                $output->writeln($l() . 'no chunks found');
            } else {
                $output->writeln($l() . count($file->getChunks()) . ' chunks found');

                foreach ($file->getChunks() as $chunk) {
                    $l = $level(1);
                    $output->writeln($l()
                                     . $chunk->getType()->getExtension()
                                     . sprintf(' chunk %s: %d assets',
                                               $chunk->getPosition(),
                                               count($chunk->getAssets())));

                    foreach ($chunk->getAssets() as $asset) {
                        $l = $level(2);
                        $output->write($l() . $asset->getVirtualPath() . ' -> ');
                        if ($asset instanceof InlineAsset) {
                            $output->writeln('(inline asset)');
                        } else {
                            $realPath = (string)$asset->getRealPath();
                            $output->writeln($realPath . ' ' . (file_exists($realPath) ? '' : '[-]'));
                        }
                    }
                }
            }
        }
    }
}