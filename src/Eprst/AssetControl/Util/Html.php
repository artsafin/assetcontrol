<?php


namespace Eprst\AssetControl\Util;

/**
 * Class Html
 *
 * @method static Html script($attrs = [])
 * @method static Html link($attrs = [])
 *
 * @method Html _src($value)
 * @method Html _rel($value)
 * @method Html _href($value)
 * @method Html _type($value)
 */
class Html
{
    /**
     * @var string
     */
    private $tag;

    /**
     * @var array
     */
    private $attrs;
    private $content;

    public function __construct($name, $attrs = [], $content = null)
    {
        $this->tag = $name;
        $this->attrs = $attrs;
        $this->content = $content;
    }

    private function attr($key, $value)
    {
        $this->attrs[$key] = $value;
    }

    public function __call($name, $arguments)
    {
        if ($name[0] == '_' && count($arguments)) {
            $this->attr(substr($name, 1), $arguments[0]);
        }

        return $this;
    }

    function __toString()
    {
        if ($this->tag === null) {
            return $this->content ? : '';
        } else {
            $attrsStrArr = [];
            foreach ($this->attrs as $k => $v) {
                $attrsStrArr[] = sprintf('%s="%s"', strtolower($k), $v);
            }

            return sprintf('<%1$s%2$s>%3$s</%1$s>', $this->tag,
                           $attrsStrArr ? ' ' . implode(' ', $attrsStrArr) : '', $this->content ?: '');
        }
    }

    public static function __callStatic($name, $arguments)
    {
        $attrs = isset($arguments[0]) ? (array)$arguments[0] : [];
        return new self($name, $attrs);
    }
}