<?php

namespace Eprst\AssetControl\App\Build;

use Eprst\AssetControl\Aom\Asset\Asset;
use Eprst\AssetControl\Aom\Chunk;
use Eprst\AssetControl\Util\Path;
use Eprst\AssetControl\Util\Position;

class ChunkBuilder
{
    /**
     * @var string
     */
    private $text;

    /**
     * @var Asset[]
     */
    private $assets = [];

    /**
     * @var Position
     */
    private $pos;

    /**
     * @var Path|null
     */
    private $chunkRelativeTo;

    public function getText()
    {
        return $this->text;
    }

    /**
     * @param string $text
     *
     * @return ChunkBuilder
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    public function setPosition(Position $pos)
    {
        $this->pos = $pos;

        return $this;
    }

    /**
     * @param Asset[] $assets
     *
     * @return ChunkBuilder
     */
    public function addAssets($assets)
    {
        $this->assets = array_merge($this->assets, $assets);

        return $this;
    }

    /**
     * @return Chunk[]
     */
    public function build()
    {
        if ($this->assets) {
            return [new Chunk($this->pos, $this->assets)];
        }
        return [];
    }

    /**
     * @return Path|null
     */
    public function getChunkRelativeBasePath()
    {
        return $this->chunkRelativeTo;
    }

    public function configure($params)
    {
        $this->chunkRelativeTo = isset($params['relative-to']) ? new Path($params['relative-to']) : null;

        return $this;
    }
}